import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AboutComponent} from './component/about/about.component';
import {HomeComponent} from './component/home/home.component';
import {HeroesComponent} from './component/heroes/heroes.component';
import {HeroeComponent} from './component/heroe/heroe.component';
import { BuscadorComponent } from './component/buscador/buscador.component';

const routes: Routes = [
{
	path:'home',
	component:HomeComponent
},

{
	path:'about',
	component:AboutComponent
},

{
	path:'heroes',
	component:HeroesComponent
},
{
	path:'heroe/:id',
	component:HeroeComponent
},
{
	path:'buscar/:termino',
	component:BuscadorComponent
},

{
	path:'**',
	pathMatch:'full',
	redirectTo:'home'
}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class RouteRoutingModule { }
